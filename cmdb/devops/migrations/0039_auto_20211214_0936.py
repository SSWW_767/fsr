# Generated by Django 2.0.6 on 2021-12-14 09:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devops', '0038_auto_20211201_1355'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='charthistory',
            index=models.Index(fields=['utime'], name='devops_char_utime_39e175_idx'),
        ),
    ]
