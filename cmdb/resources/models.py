from django.db import models
from products.models import Product


# Create your models here.


class ServerGroup(models.Model):
    name = models.CharField(max_length=80,blank=True, null=True,unique=True,verbose_name="主机组名称")
    info = models.CharField(max_length=200, blank=True, null=True, verbose_name="备注")
    utime = models.DateTimeField(auto_now=True, null=True, verbose_name="添加时间")

class NewServer(models.Model):
    hostname = models.CharField(max_length=64, verbose_name='主机名')
    ip_inner = models.CharField(max_length=32, verbose_name='连接IP地址', unique=True)
    port = models.IntegerField(verbose_name='连接端口')
    os_status_list = [
        (0, 'Linux'),
        (1, 'Windows'),
        (2, 'Mac')
    ]
    os_status = models.IntegerField(choices=os_status_list, verbose_name='操作系统')
    system_status_list = [
        (0, '虚拟机'),
        (1, '物理机')
    ]
    system_status = models.IntegerField(choices=system_status_list, verbose_name='机器类型')
    cpu_info = models.CharField(max_length=64, verbose_name='CPU型号')
    cpu_count = models.IntegerField(verbose_name='CPU物理个数')
    mem_info = models.CharField(max_length=32, verbose_name='内存信息')
    os_system = models.CharField(max_length=32, verbose_name='系统平台')
    os_system_num = models.IntegerField(verbose_name='系统平台位数')
    uuid = models.CharField(max_length=64, verbose_name='UUID')
    sn = models.CharField(max_length=64, verbose_name='SN')
    scan_status_list = [
        (0, '连接异常'),
        (1, '连接正常')
    ]
    scan_status = models.IntegerField(choices=scan_status_list, verbose_name='探测状态')
    chart_status = models.BooleanField(default=False, verbose_name="图表开启状态")
    create_date = models.DateTimeField(auto_now_add=True, verbose_name='创建主机时间')
    update_date = models.DateTimeField(auto_now=True, verbose_name='更新主机时间')
    server_user = models.ForeignKey('ServerUser', null=True, on_delete=models.SET_NULL,related_name='serveruser', verbose_name='管理用户')
    server_group = models.ForeignKey('ServerGroup', null=True, on_delete=models.SET_NULL,related_name='servergroup', verbose_name='主机组')

class ServerUser(models.Model):
    name = models.CharField(max_length=32, verbose_name='名称')
    username = models.CharField(max_length=32, verbose_name='系统用户')
    password = models.CharField(max_length=64, verbose_name='系统密码')
    privatekey = models.TextField(default=None,verbose_name='密钥')
    pubkey = models.TextField(default=None, verbose_name='公钥')
    info = models.TextField(verbose_name='备注')

    class Meta:
        default_permissions = []
        permissions = (
            ('add_serveruser', '添加资产用户'),
            ('delete_serveruser', '删除资产用户'),
            ('view_serveruser', '查看资产用户'),
            ('modify_serveruser', '修改资产用户'),
        )
